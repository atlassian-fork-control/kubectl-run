import base64
import os
import sys
import subprocess

import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger


logger = get_logger()

schema = {
    'KUBE_CONFIG': {'type': 'string', 'required': True},
    'KUBECTL_COMMAND': {'type': 'string', 'required': True},
    'KUBECTL_ARGS': {'type': 'list', 'required': False, 'default': []},
    'RESOURCE_PATH': {'type': 'string', 'required': False, 'nullable': True, 'default': ''},
    'LABELS': {'type': 'list', 'required': False},
    'WITH_DEFAULT_LABELS': {'type': 'boolean', 'required': False, 'default': True},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class KubernetesDeployPipe(Pipe):

    def update_labels_in_metadata(self, template, labels):
        with open(template, 'r') as template_file:

            yamls = list(yaml.safe_load_all(template_file.read()))
            for yaml_doc in yamls:
                yaml_doc['metadata'].setdefault('labels', {}).update(labels)

        with open(template, 'w') as template_file:
            yaml.dump_all(yamls, template_file)

    def handle_apply(self):
        resource_path = self.get_variable('RESOURCE_PATH')
        if not resource_path:
            self.fail('Path to the spec file is required when running the "apply" command')

        labels = dict(self.get_variable('LABELS'))
        if self.get_variable('WITH_DEFAULT_LABELS'):
            bitbucket_branch = os.getenv('BITBUCKET_BRANCH')
            if bitbucket_branch is not None and '/' in bitbucket_branch:
                self.log_warning('"/" is not allowed in kubernetes labels. Slashes will be replaced by a dash "-" in the "bitbucket.org/bitbucket_commit" label value.'
                                 'For more information you can check the official kubernetes docs'
                                 'https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set')
                labels['bitbucket.org/bitbucket_branch'] = bitbucket_branch.replace('/', '-')

            step_triggerer_uuid = os.getenv('BITBUCKET_STEP_TRIGGERER_UUID')
            if step_triggerer_uuid is not None:
                # trim the starting and closing curly braces
                labels['bitbucket.org/bitbucket_step_triggerer_uuid'] = step_triggerer_uuid[1:-1]

            variables = [
                ('BITBUCKET_COMMIT', 'bitbucket.org/bitbucket_commit'),
                ('BITBUCKET_REPO_OWNER', 'bitbucket.org/bitbucket_repo_owner'),
                ('BITBUCKET_REPO_SLUG', 'bitbucket.org/bitbucket_repo_slug'),
                ('BITBUCKET_BUILD_NUMBER', 'bitbucket.org/bitbucket_build_number'),
                ('BITBUCKET_DEPLOYMENT_ENVIRONMENT', 'bitbucket.org/bitbucket_deployment_environment')
            ]

            for variable, label_name in variables:
                value = os.getenv(variable)
                if value is not None:
                    labels[label_name] = value
        is_path_dir = os.path.isdir(resource_path)

        if is_path_dir:
            templates = [template for template in os.listdir(resource_path) if template.endswith(('.yml', '.yaml'))]
            templates_absolute_paths = [os.path.join(resource_path, template_file) for template_file in templates]
        else:
            templates_absolute_paths = [resource_path]

        for template_file in templates_absolute_paths:

            self.update_labels_in_metadata(template_file, labels)

        debug = ['--v=2'] if self.get_variable('DEBUG') else []

        result = subprocess.run(['kubectl', 'apply', '-f', resource_path] + list(self.get_variable('KUBECTL_ARGS')) + debug,
                                stdout=sys.stdout)

        if result.returncode != 0:
            self.fail('kubectl apply failed.')
        else:
            self.success('kubectl apply was successful.')

    def handle_generic(self, cmd):
        debug = ['--v=2'] if self.get_variable('DEBUG') else []
        result = subprocess.run(['kubectl'] + cmd.split() + list(self.get_variable('KUBECTL_ARGS')) + debug,
                                stdout=sys.stdout)

        if result.returncode != 0:
            self.fail(f'kubectl {cmd} failed.')
        else:
            self.success(f'kubectl {cmd} was successful.')

    def configure(self):
        with open('config', 'w+') as kube_config:
            kube_config.write(base64.b64decode(self.get_variable('KUBE_CONFIG')).decode())

        os.environ['KUBECONFIG'] = 'config'

    def run(self):

        self.configure()

        cmd = self.get_variable('KUBECTL_COMMAND')

        if cmd == 'apply':
            self.handle_apply()
        else:
            self.handle_generic(cmd)

        self.success(message=f"Pipe finished successfully!")


if __name__ == '__main__':
    pipe = KubernetesDeployPipe(schema=schema, check_for_newer_version=True)
    pipe.run()
