FROM python:3.7-slim

COPY requirements.txt /

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN pip install -r requirements.txt && \
    apt-get update && apt-get install --no-install-recommends -y \
      apt-transport-https=1.8.2 \
      gnupg=2.2.12-1+deb10u1 \
      curl=7.64.0-4+deb10u1 && \
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list && \
    apt-get update && \
    apt-get install --no-install-recommends -y kubectl=1.17.0-00 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["python", "/pipe.py"]
